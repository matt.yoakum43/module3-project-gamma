import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function NoDataNavigate({ fromStats, fromSell }) {
  const navigate = useNavigate();

  const stockNavError = {
    stockNavError: fromStats,
  };
  const noStocksAccessingSellForm = {
    noStocksAccessingSellForm: fromSell,
  };

  useEffect(() => {
    navigate(
      "/portfolio",
      fromStats
        ? { state: stockNavError }
        : { state: noStocksAccessingSellForm }
    );
  });
}
