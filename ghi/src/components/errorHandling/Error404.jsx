import Error404Gif from "../../media/gifs/404wiz.gif";
import { useGetAccountQuery } from "../../app/portfolioApi";

export default function Error404() {
  const { data: isLoggedIn } = useGetAccountQuery();
  return (
    <>
      <div className="container card shadow bg-white mt-4">
        <div className="align-middle">
          {!isLoggedIn ? (
            <>
              <h2 className="container text-center fw-light pt-4">
                Hold on to your hat, that route does not exist!
              </h2>
              <h2 className="text-center fw-light">Are you logged in?</h2>
            </>
          ) : (
            <h2 className="container text-center fw-light pt-4">
              Hold on to your hat, that route does not exist!
            </h2>
          )}
        </div>
        <div className="text-center">
          <img src={Error404Gif} alt="404 Wizard"></img>
        </div>
      </div>
      <div className="clear"></div>
    </>
  );
}
