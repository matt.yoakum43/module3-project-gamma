import { useGetPortfolioQuery } from "../../app/portfolioApi";
import GetPerformances from "./GetPerformances";
import loadingGif from "../../media/gifs/newwizard.gif";
import NoDataNavigate from "../errorHandling/NoDataNavigate";

function Stats() {
  const { data: stocks, isLoading } = useGetPortfolioQuery();

  if (isLoading) {

    return (
      <>
        <div className="container">
          <img
            className="p-40 col-30 position-absolute top-50 start-50 translate-middle img-responsive"
            src={loadingGif}
            alt="wait until the page loads"
          />
        </div>
      </>
    );
  } else {
    return (
      <>
        {stocks.stocks.length > 0 ? <GetPerformances stocks={stocks.stocks} /> : <NoDataNavigate fromStats={true} />}
      </>
    )
  }
}

export default Stats;
