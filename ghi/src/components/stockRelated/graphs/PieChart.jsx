import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from "recharts";

function Chart1({ stocks }) {
  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 1.2;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="black"
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  const colors = ["#99e2b4", "#88d4ab", "#78c6a3", "#67b99a", "#56ab91", "#469d89", "#358f80", "#248277", "#14746f", "#036666"];

  return (
    <>
      <ResponsiveContainer width="100%" minHeight={400}>
        <PieChart>
          <Pie
            data={stocks}
            dataKey="total"
            nameKey="symbol"
            fill="#8884d8"
            paddingAngle={1}
            minAngle={1}
            label={renderCustomizedLabel}>


            {stocks.map((entry, index) =>
              <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
            )}
          </Pie>
          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
    </>
  );
}

export default Chart1;
