import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  handleSymbolChange,
  handleSharesChange,
  errorChange,
  reset,
} from "../../features/portfolioSlice";
import {
  useGetPortfolioQuery,
  useCreateSellMutation,
} from "../../app/portfolioApi";
import ErrorMessage from "../errorHandling/ErrorMessage";
import { useNavigate } from "react-router-dom";
import sellSound from "../../media/sounds/sell_sound.mp3";
import errorSound from "../../media/sounds/ouch.mp3";
import NoDataNavigate from "../errorHandling/NoDataNavigate";

export default function SellForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { errorMessage, fields } = useSelector((state) => state.newStock);
  const { data: stocks, isLoading } = useGetPortfolioQuery();
  const [createSell] = useCreateSellMutation();
  const audio = new Audio(sellSound);
  const errorAudio = new Audio(errorSound);

  useEffect(() => {
    dispatch(reset())
  }, [dispatch])

  const handleSubmit = (e) => {
    e.preventDefault();
    if (fields.shares <= 0) {
      errorAudio.play();
      dispatch(errorChange("Shares must be positive numeric value"))
      return
    }
    createSell({ symbol: fields.symbol, shares: fields.shares })
      .unwrap()
      .then((payload) => {
        dispatch(reset())
        navigate("/portfolio")
        audio.play()
      })
      .catch((error) => {
        dispatch(errorChange(error.data.detail));
        errorAudio.play();
      });
  };

  const sellAllStock = (e) => {
    e.preventDefault();
    if (!fields.symbol) {
      errorAudio.play();
      dispatch(errorChange("Please choose a stock"))
      return
    }
    audio.play();
    for (let userStock of stocks.stocks) {
      if (userStock.symbol === fields.symbol) {
        createSell({ symbol: userStock.symbol, shares: userStock.shares })
      }
    }
    dispatch(reset())
    navigate("/portfolio")
  };

  if (!isLoading) {
    return (
      <>
        <div className="container ">
          <div className="shadow p-4 col-3 position-absolute top-50 start-50 translate-middle bg-white rounded border border-2">
            <h5 className="card-title text-primary font-weight-bold">SELL</h5>
            <hr />
            <form onSubmit={handleSubmit}>
              {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
              <div className="mb-3">
                <select
                  value={fields.symbol}
                  onChange={(e) => dispatch(handleSymbolChange(e.target.value))}
                  name="symbol"
                  id="symbol"
                  className="form-select"
                  required
                >
                  <option value="">Choose a stock</option>
                  {stocks.stocks.length > 0 ? stocks.stocks.map((stock) => {
                    return (
                      <option value={stock.symbol} key={stock.id}>
                        {stock.symbol}
                      </option>
                    );
                  }) : <NoDataNavigate fromSell={true} />}
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="Login__password" className="form-label">
                  Shares:
                </label>
                <input
                  className="form-control form-control-sm"
                  type={`number`}
                  id="Login__password"
                  value={fields.shares}
                  onChange={(e) => dispatch(handleSharesChange(e.target.value))}
                />
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-primary text-white mx-2">
                  SELL
                </button>
                <button type="button" className="btn btn-outline-primary mx-2" onClick={sellAllStock}>Sell All {fields.symbol ? fields.symbol : "stock"}</button>
              </div>
            </form>
          </div >
        </div >
      </>
    );
  } else {
    return (
      <>
        <div className="container ">
          <div className="shadow p-4 col-3 position-absolute top-50 start-50 translate-middle bg-white rounded border border-2">
            <h5 className="card-title text-primary font-weight-bold">Sell</h5>
            <hr />
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <select className="form-select" required>
                  <option value="">Loading...</option>
                </select>
              </div>
              <div className="mb-3">
                <label htmlFor="Login__password" className="form-label">
                  Shares:
                </label>
                <input
                  className="form-control form-control-sm"
                  type={`number`}
                  id="Login__password"
                  value={fields.shares}
                  onChange={(e) => dispatch(handleSharesChange(e.target.value))}
                />
              </div>
              <div className="text-center">
              </div>
            </form>
          </div>
        </div>
      </>
    );
  }
};
