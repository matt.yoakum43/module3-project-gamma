import { useLogoutMutation } from "../../app/portfolioApi";

const Logout = () => {
  const [logout] = useLogoutMutation();
  return (
    <button className="nav-link text-white newbutton ghost mx-4 px-4 fw-bold" onClick={logout}>
      Logout
    </button>
  );
};

export default Logout;
