import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  fields: {
    symbol: "",
    shares: 0,
  },
  errorMessage: null,
};

export const newStockSlice = createSlice({
  name: "newStock",
  initialState,
  reducers: {
    handleSymbolChange: (state, action) => {
      state.fields.symbol = action.payload;
    },
    handleSharesChange: (state, action) => {
      state.fields.shares = action.payload;
    },
    errorChange: (state, action) => {
      state.errorMessage = action.payload;
    },
    reset: () => initialState,
  },
});

export const {
  handleSymbolChange,
  handleSharesChange,
  errorChange,
  reset,
} = newStockSlice.actions;

export default newStockSlice.reducer;
