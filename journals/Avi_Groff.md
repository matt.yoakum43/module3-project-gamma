3/27
We'll need some logic somewhere to distinguish between whether we're
trying to create a new stock or update a stock that already exists in
the user's portfolio.
We'll have to perform a get on that stock to see if it exists in
user's portfolio before we decide whether we post or put.

We'll need to decide wether we'll user stock_symbol or stock_id for
all of our routes. For consistency it might make more sense to use
symbol for consistency sake since we'll have to use it for the routes
that only talk to the third party api.

update not working because the ids aren't strings rather they're
objects or something I THINK?
SOLVED that turned out to be the problem

We should move the api call into its own function that takes in the symbol and outputs a dictionary with all the relevant data
SOLVED

4/3
Added hardcoded test paths to fast api to test redux without worrying about fastapi and react authentication

TODO
Add try/except to all of the router in our backend

4/4
Added a login page using redux

4/5
Succesfully fetched data from the createapi to populate the user's portolio page on the front-end

4/6
Created new fastapi routes to handle cash total inside the user's portfolio

4/7
Added link to portoliolist.js and uselocation at stockpage.js in order to pass data from
the portfolio to the stockpage.

SPRING BREAK 4/8 - 4/16 SPRING BREAK
Created a barebones implementation of the buy form just to start getting stock
data into the back end and start messing with the idea of changing how much
cash the user has whenever they buy a new stock.

4/17
Worked on debugging Buy form as well as experimenting with invalid logins.

4/18
Created signup form for front end. As well as createapi endpoints to add the user to
the backend.

Worked on adding cash to the user's portfolio ('stock' collection) upon signup via the
front-end. We'll be moving this function to the backend.

4/19
Worked on reducing latency within portfolio page to reflect changes within the user's
portfolio whenever buying/selling a stock occurs.

Added cash to the user's 'stock' collection upon signup via the backend.

4/20
Fixed buy form bug where /api/price/{symbol} endpoint would be called every single time the form state changed.
To fix this I implemented conditional fetching to the useGetPriceQuery endpoint.
Fetching should only occur when the form is submitted and should stop once data has been
recieved by the endpoint.

Also fixed bug courtesy of our third party yfinance api where stocks that used to have
'ask' price stopped having ask price. So now we check for 'currentprice' as well.

4/24
Worked on doing more debugging with buy form to remove errors in front end dev tools.
Specifically controlling when the different endpoints are triggered.

4/25
completely moved buy form logic from the front end to the backend.
This includes updating the user's cash
creating a new entry in the history collection
creating/updating/deleting an entry in the portfolio collection

4/26
Formatted history transaction dates to reflect user's timezone
Corrected tag invalidation when user buys a stock orderhistory should be invalidated.

4/27
Added two more unit tests
Added more criteria to user signup
username must be greater than 3 characters but less than 15
password field may not be empty

4/28
Fixed buy form 422 error if no shares are inputted.
Error dispatch fix.
Removed minor redundancy with buy/sell forms.
Reorganized ALL files into nicely named foldersand fixed imports because of that.
Fixed most errors that aren't related to images needing an alt tag.
Implemented fix for a user trying to access sell form without having stocks in their portfolio.
Refactored navigate error component to be more modular.
