* 03/20/23 - 03/24/23: Project kick off
wireframe ("Smart Stocks" app, "The premiere solution for stocks portfolio management!"), endpoints and database choice (MongoDB)

-----------------------------------------------------------------------------

* 03/27/23: worked with team
- updated endpoints
- sat up Mongo db in Docker compose file
- defined our first API routes with links to our third party API (yahoo finance)
- added stocks routes (get all, get one, create, update, delete)

-----------------------------------------------------------------------------

* 03/28/23: worked with team
- added authentification
- added history methods (get all and create)

-----------------------------------------------------------------------------

* 3/29/23: worked with team
- finished up "history" routes
- added "news" routes

-----------------------------------------------------------------------------

* 3/30/23: worked with team
- finished up "news" routes
- added "stocks/performance" route
- started to configure front end with new packages
- added bootstrap
- organized src folder
- basic functions displaying home page with navbar, footer and text in body
- worked more on footer: added links to personal info

-----------------------------------------------------------------------------

* 4/04/23: worked with team
- tested out redux
- practiced on dummy branch to add list of stocks and create form

-----------------------------------------------------------------------------

* 4/04/23: worked with team & solo
- started the front-end authentification using redux (login page)
- added graphs using Rechart (1 pie chart and 1 line chart to start with)
- fixed footer position depending on the page we are on
- removed all custom css and only using bootstrap
- restyled login page

-----------------------------------------------------------------------------

* 04/05/23: worked with team, in pairs & solo
- restyled login page
- restyled main page
- made y-axis dynamic in line graph
- restyled footer with "contact us" link to our emails
- made Bootstrap responsive (able to edit all colors now with our own color palette)
- fixed footer syntax
- started to integrate new routes with "cash" (get / post / update) (Jakob and I)
- started linking data on frontend with backend (portfolio page) (Avi and Matt)

-----------------------------------------------------------------------------

* 04/06/23: worked with team, in pairs & solo
- using Redux all along now
- did "stock" page displaying info on stock from portfolio / graph with price over time over a month / news from API
- restyled "stock" page
- new routes for "cash" (Avi + Jakob)
- added wizard during loading time on "stock" page ^^

-----------------------------------------------------------------------------

* 04/07/23: worked with team, in pairs & solo
- added news, pie chart and restyled paged on "portfolio" main page
- added custom background to pages
- linked portfolio page to stock page by assing in arguments to dynamically display data on stock page
- updated navbar
- updated graphs styles

-----------------------------------------------------------------------------

* 04/10/23 - 04/14/23: break - solo minor adds
- started to style mainpage (me)
- added history basic page (Jakob)
- added buy form (Avi)

-----------------------------------------------------------------------------

* 04/17/23: worked with team & solo
- created login form:
    - error handling
    - alert window pop up
    - if success, redirect to home page
- work in progress on seperate branch for now to implement the stats page:
    - added enpoint in back to get "performances" for all stocks
    - added nested hooks to get "performances" for all stocks
    - in pause for now: data available on "stats" page, need to work on the maths now and think about the graph

-----------------------------------------------------------------------------

* 04/18/23: worked with team & in pairs
- created sign up form
- created logout
- created sell form
- fixed long time errors with "doctype error json not recognized": had to remove the "galvannize auth" import and tags from App.js

-----------------------------------------------------------------------------

* 04/19/23: worked with team & solo
- updated navbar (Matt)
- added cash power, gains, losses to portfolio and display data (me)
- added cash to 10k during signup (Avi)
- fixed async problem when loading porfolio (Team)
- installed latest version to yahoo finance to fix authorization problem to third party API (Team)
- restyled and formatted numbers on portfolio and single stock pages with green/red arrows when going up/down (me)

-----------------------------------------------------------------------------

* 04/20/23: worked with team & solo
- added modal on stockpage when clicking on "buy" button (me)
- added form to buy shares for specific stock in newly created modal (me)
- fixed errors in buyForm (Avi)
- updated Navbar & styling (Matt)
- updated signup: showing up when logged out as well (Jakob)
- fixed API new release bugs in back when getting prices (Avi)
- added new endpoint in back to "getPerformances" and prepared data ready for analysis on "Stats" page on front (me)
- added "show/hide news" button on portfolio page (Jakob)

-----------------------------------------------------------------------------

* 04/24/23: worked with team & solo
- added "sell" modal on the stockPage (me)
- made "sell" modal functional, working when clicked on "sell" and sending alarm if cannot sell (me)
- kept working on the stats page (work in progress, not complete) (me)
- debugging "buy" form page (some async issues needed to refresh page) (Avi)
- restyling some pages and sending 404 pages, fixing console.log bugs (Jakob)
- working on a carousel on the Main Page (Matt)

-----------------------------------------------------------------------------

* 04/25/23: worked with team & solo
- added "stats" page plotting the portfolio performances over 1 year (me)
- restyled footer a bit to take less space on screen (me)
- working on fixing problem when trying to access the "stock" page from the browser to show our custom 404 page (Jakob)
- switched code of the BuyForm page from the front to the back (Avi)
- updated buy & sell modals in StockPage with buy & sell codes switched to back (me)
- completed stats page with buttons hide/show, plotting performances over the day, month and year (me)
- added different colors to Pie chart (Jakob & Matt)
- working on styling the Main page (Matt)

-----------------------------------------------------------------------------

* 04/26/23: worked with team & solo
- added unit tests for portfolio and history (team)
- fixed coupld of warnings and erros popping up on pages for edge cases (team)
- restyled main, history and portfolio pages (me)

-----------------------------------------------------------------------------

* 04/27/23: worked with team & solo
- added sounds effects when buyig/selling (me)
- added pop up modal with sound effects and gif when signing up (me)
- restyled navbar completely (me)
- restyled 404 pages with updated gif and page format (me)
- updated footer a bit (me)
- sell all buttons (Avi)
- additional unit tests (Avi)
- added features with username displayed when logged in (Jakob)
- fixed bugs and warnings (Jakob)
- updated logo (me)
- restyled buy/sell forms a bit (me)
- personalized "react tag" to our app (me)

-----------------------------------------------------------------------------

* 04/28/23: worked with team & solo
- added scroll up button
- fixed margin side on main page
- cleaned up code
- fixed modals alerts
- added bug sounds
