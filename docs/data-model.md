# Data models


---

### Stock(StockParams)

| name             | type   |
| ---------------- | ------ |
| name             | string |
| price            | float  |
| total            | float  |
| id               | string |
| user_id          | string |

### StockIn(BaseModel)

| name             | type   |
| ---------------- | ------ |
| id               | string |
| shares           | float  |

### StockParams(BaseModel)

| name             | type   |
| ---------------- | ------ |
| symbol           | string |
| shares           | float  |

### StockWithNews(Stock)

| name             | type   |
| ---------------- | ------ |
| news             | string |

### History(Stock)

| name             | type     |
| ---------------- | -------- |
| transaction_type | string   |
| date             | datetime |

### HistoryParams(StockParams)

| name             | type   |
| ---------------- | ------ |
| transaction_type | string |

### News(BaseModel)

| name             | type   |
| ---------------- | ------ |
| title            | string |
| link             | string |
| thumbnail        | string |
| publisher        | string |
| type             | string |

### Performance(BaseModel)

| name             | type     |
| ---------------- | -------- |
| date             | datetime |
| close            | float    |

### Performances(BaseModel)

| name             | type     |
| ---------------- | -------- |
| date             | datetime |
| close            | float    |
| symbol           | string   |

### Cash(BaseModel)

| name             | type     |
| ---------------- | -------- |
| total            | float    |

### Price(BaseModel)

| name             | type     |
| ---------------- | -------- |
| symbol           | string   |
| price            | float    |

### AccountOut(BaseModel)

| name             | type     |
| ---------------- | -------- |
| id               | string   |
| username         | string   |

### AccountIn(BaseModel)

| name             | type     |
| ---------------- | -------- |
| username         | string   |
| password         | string   |

### AccountOutWithHashedPassword(AccountOut)

| name             | type     |
| ---------------- | -------- |
| hashed_password  | string   |

### AccountForm(BaseModel)

| name             | type     |
| ---------------- | -------- |
| username         | string   |
| password         | string   |
