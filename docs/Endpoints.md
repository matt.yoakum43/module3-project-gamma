### Get all data from user's stock portfolio table

* Endpoint path: api/stocks/
* Endpoint method: GET
* Query parameters:
  * user_id: grab all stocks for that user
* token comes in as a header and it will be passed in the path

* Response: A list of rows (dictionaries) from the portfolio table

* Response shape (JSON):
    {
	stocks:
	[
	{name: string, symbol: string, shares:int, price: float, total: float},
	]
     }

### Get all data from user's order history table

* Endpoint path: /api/orderhistory/
* Endpoint method: GET
* Query parameters:
  * user_id: Get that specific user's order history information

* Response: A list of stock orders (buying and selling) made by the user
* Response shape (JSON):
	{
	history:
	[
	{name: string, symbol: string, shares:int, price: float, total: float, date: date, transaction_type: 	string},
	]
     }

### Update a single stock from user's portfolio (when buying/selling)

* Endpoint path: api/stocks/{stock_symbol}
* Endpoint method: PUT
* Query parameters:
  * user_id: The user whos stock we're trying to update
  * stock_symbol: the stock we're trying to update


* Request shape (JSON):
	{
	shares:int
	}


* Response: Retrieve the name, stock symbol, and number of shares updated
* Response shape (JSON):
	{
	user_id: int, MAYBE
	name: string,
	symbol: string,
	shares:int,
	}


### Get news information for all of users stocks

* Endpoint path: api/news/
* Endpoint method: GET
* Query parameters:
  * user_id: grab all news related to that user's stocks


* Response: list of news dictionaries containing
		title, description, image, link or body related to news articles.
		All pertaining to stocks in that user's portfolio

* Response shape (JSON):
	{
	news: [
		{
		"title":string,
		"description":text,
		"image":url,
		"body":text, MAYBE
		OR
		"link": url,
		},
	}


### Delete a user's stock when they sell all of that stock

* Endpoint path: /api/stocks/{stock_symbol}
* Endpoint method: DELETE
* Query parameters:
  * user_id: The user whos stock we're trying to delete
  * stock_symbol: the stock we're trying to delete

* Response: the deleted status when trying to delete
* Response shape (JSON):
	{
	deleted: boolean
	}


### Create a new entry into the user's portfolio

* Endpoint path: /api/stocks/
* Endpoint method: POST
* Query parameters:
  * user_id: The user we want to associate with the data

* Request shape (JSON):
	{
	name:string,
	symbol:string,
	shares:int,
	}
	NOT SURE IF WE NEED user_id IF ITS PASSED IN VIA THE PATH

* Response: «Human-readable description
            of response»
* Response shape (JSON):
	{
	name:string,
	symbol:string,
	shares:int,
	}

### Create new entry/row into user's order history table

* Endpoint path: api/history
* Endpoint method: POST
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
	{
	Name: string,
	symbol: string,
	shares: string,
	price: float,
	transaction_type: string
	}

* Response: Show the new entry in the table including
		Name, symbol, shares, price, and transaction type (buy/sell)
* Response shape (JSON):
	{
	Name: string,
	symbol: string,
	shares: string,
	price: float,
	transaction_type: string
	}


### Get all data from user's order history table

* Endpoint path: api/history
* Endpoint method: GET
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Response: List of all orders that user has made
* Response shape (JSON):
	{
		orders: [
		{
		Name: string,
		symbol: string,
		shares: string,
		price: float,
		transaction_type: string
		},
		{
		Name: string,
		symbol: string,
		shares: string,
		price: float,
		transaction_type: string
		}
		]
	}


### Get pricing information for a specific interval for a specific stock for graphing (1 month etc) (Third Party)

* Endpoint path: api/performance/
* Endpoint method: GET
* Query parameters:
  * stock symbol: name of the stock
  * period: starting date from current day

* Headers:
  * Authorization: Bearer token

* Response: Lists pricing for specific stock over time period
* Response shape (JSON):
	{
		Name: string,
		symbol: string,
		price: float,
		date: date
	},
	...
	{
		Name: string,
		symbol: string,
		price: float,
		date: date
	}


### Create new user

* Endpoint path: api/users/
* Endpoint method: POST
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
	{
		email: string,
		password: string,
	}

* Response: Show the new entry in the table including
		email, password, id
* Response shape (JSON):
	{
		email: string,
		password: string,
	}


### Update user information

* Endpoint path: api/users/
* Endpoint method: PUT
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
	{
		email: string,
		password: string,
	}

* Response: Show the new entry in the table including
		email, password, id
* Response shape (JSON):
	{
		email: string,
		password: string,
	}


### «Human-readable of the endpoint»

* Endpoint path: «path to use»
* Endpoint method: «HTTP method»
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```json
    «JSON-looking thing that has the
    keys and types in it»
    ```

* Response: «Human-readable description
            of response»
* Response shape (JSON):
    ```json
    «JSON-looking thing that has the
    keys and types in it»
    ```
