1. \***\* Add try/excepts to backend \*\***
2. Organizing components into correct folders/update import paths
3. Figure out how to stay logged in instead of the token "expiring" or whatever after a few minutes
4. ~~Maybe come up with error handling who's stock price comes in at zero (new problem we had when we tried to use the stock LYSFF)~~
5. ~~\***\* Maybe make a successful buy redirect you to another page or have it return some kind of a succesful response \*\***~~
6. ~~Add logic to govern buy/sell operations with cash~~
7. ~~Add Cash to portfolio list~~
8. ~~NEED TO ADD A WAY TO HANDLE WHEN STOCKPAGE.JS IS MISSING THE THUMBNAIL FOR A NEWS ARTICLE SOLVED~~
9. Add calendar to the date search in order history
10. ~~Add signup form along with creating cash for the user~~
11. Make login error message look the same as signup error message
12. ~~\***\* add dropdown to sell form \*\***~~
13. \***\* Fix errors popping up in console/containers \*\***
14. ~~Order history page by date~~
15. ~~Have warnings when user tries to sell more than they have or buy more than they can afford~~
16. Add a welcome message to the top of the website saying hi user!
17. Remove all console.logs and all prints (at the very end)
18. Check code quality (Blake for Python and Format doc for JavaScript) (at the very end)
19. make sure code is DRY (especially in .js files, whenever jsx repeats itself, make functions instead)
20. format/finalze main page (disclaimer for Yahoo finance + project descriptions + notes about us)
21. ~~update footer with yahoo disclaimer maybe?~~
22. add stats page
23. format time in history so that it reflects EST time at least (NY stock exchange)?
    Just need to add number of minutes difference between EST and UTC to the result of .valueof
    If you specifically want EST in some places
24. ~Usegetperfomances, portfolio Alix needs~
25. ~Add tests for endpoints!!! (4 minimum)~
26. Add Readme to describe project.
27. Consider adding something that shows the most popular stocks somewhere or something that gives the user an example stock to input to buy
28. Make the Order history page more responsive
