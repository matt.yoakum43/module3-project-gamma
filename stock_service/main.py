from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import stocks, accounts, history
from authenticator import authenticator

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", None),
        "http://localhost:3000",
        "https://hrgroup4.gitlab.io",
        "https://hrgroup4.gitlab.io/wealth-wizard",
        os.environ.get("REACT_APP_STOCK_SERVICE_API_HOST", None),
        os.environ.get("PUBLIC_URL", None)

    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(authenticator.router, tags=["Auth"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(stocks.router, tags=["Stocks"])
app.include_router(history.router, tags=["History"])
