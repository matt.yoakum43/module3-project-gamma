from fastapi import APIRouter, Depends
from models import History, HistoryParams, HistoryList
from queries.history import HistoryQueries
from authenticator import authenticator

router = APIRouter()


@router.post("/api/history", response_model=History)
def create_history(
    params: HistoryParams,
    repo: HistoryQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(params, user_id=account_data["id"])


@router.get("/api/history", response_model=HistoryList)
def get_all_history(
    repo: HistoryQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return {"history": repo.get_all(user_id=account_data["id"])}
