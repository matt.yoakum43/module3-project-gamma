from queries.client import HistoryQueries
from models import History, HistoryParams
from datetime import datetime
import time
from queries.finance_third_party import pricing_info


class HistoryQueries(HistoryQueries):
    HISTORY_COLLECTION = "history"

    def get_all(self, user_id: str) -> list[History]:
        history = []
        for transaction in self.history_collection.find({"user_id": user_id}):
            transaction["id"] = str(transaction["_id"])
            history.append(History(**transaction))

        return history

    def create(self, params: HistoryParams, user_id: str) -> History:
        history = params.dict()
        history["symbol"] = history["symbol"].upper()
        history["date"] = datetime.now()
        history["user_id"] = user_id
        history.update(pricing_info(history["symbol"], history["shares"]))
        time.sleep(1)
        self.history_collection.insert_one(history)
        history["id"] = str(history["_id"])

        return History(**history)
